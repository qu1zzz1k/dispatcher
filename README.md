# Dispatcher Servlet

Данное приложение - каркас для решения тестового задания при приеме на работу в компанию ZenSoft.


## Задание

### Проблема

При разработке JEE приложения для каждого нового Endpoint'а необходимо создать сервлет и указать его в файле `web.xml`.

Например:

```xml
<servlet>
    <servlet-name>imagesServlet</servlet-name>
    <servlet-class>by.zensoft.ImagesServlet</servlet-class>
</servlet>
<servlet-mapping>
    <servlet-name>imagesServlet</servlet-name>
    <url-pattern>/images</url-pattern>
</servlet-mapping>
```

Таким образом дескриптор развертывания (`web.xml`) очень быстро становится нечитаемым и трудно поддерживаемым.

Особенные проблемы возникают в случае необходимости мержа веток нескольких разработчиков.


### Постановка задачи

Рассмотрев успешный опыт `Spring MVC`, мы решили реализовать схожий инструментарий по маппингу запросов.

Для этого мы создали аннотации `Controller` и `RequestMapping`.

В пакете `dispatcher.controllers` расположен пример базового контроллера `BaseController`.


Необходимо реализовать инструментарий `DispatcherServlet` таким образом, чтобы по запросу:

```
GET /hello HTTP.1.0
```

клиент получил HTML, возвращаемый методом `BaseController.hello()`.

А добавление новых контроллеров было бы настолько же очевидно и просто как и описание класса `BaseController`.


### Зависимости

Данное приложение содержит все необходимые для решения задачи зависимости. В частности, библиотеку по работе с рефлексией.

```java
Reflections reflections = new Reflections("dispatcher.*");
Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Controller.class);
```

## Условия

* На решение данной задачи отводится 30 минут времени кандидата
* Полное отсутствие подключения к сети Интернет

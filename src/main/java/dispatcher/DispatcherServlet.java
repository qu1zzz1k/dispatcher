package dispatcher;

import dispatcher.context.DispatcherContext;
import dispatcher.exceptions.EndpointNotFoundException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DispatcherServlet extends HttpServlet {

    static {
        DispatcherContext.init();
    }

    @Override
    protected void service(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        try (ServletOutputStream outputStream = resp.getOutputStream()) {
            try {
                outputStream.print(DispatcherContext.getInvocation(req.getRequestURI()).get());
            } catch (EndpointNotFoundException e) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        }
    }
}

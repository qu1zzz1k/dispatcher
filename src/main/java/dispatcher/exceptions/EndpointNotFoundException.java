package dispatcher.exceptions;

public final class EndpointNotFoundException extends Exception {
    public EndpointNotFoundException(final String message)
    {
        super(message);
    }
}

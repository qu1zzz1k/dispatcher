package dispatcher.exceptions;

public final class ContextInitializationException extends RuntimeException
{
	public ContextInitializationException(final String message)
	{
		super(message);
	}

	public ContextInitializationException(final String message, final Throwable cause) {
		super(message, cause);
	}
}

package dispatcher.context;

import dispatcher.annotations.Controller;
import dispatcher.annotations.RequestMapping;
import dispatcher.exceptions.ContextInitializationException;
import dispatcher.exceptions.EndpointNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Supplier;

import static java.lang.reflect.Modifier.isPublic;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public final class DispatcherContext {

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DispatcherContext.class);
    private static final String SEARCH_PATH = "dispatcher.*";

    private static DispatcherContext instance;
    private final Map<String, Method> mappingContext;
    private final Map<Class, Object> controllersMap;

    private DispatcherContext(final Map<Class, Object> controllersMap) {
        this.mappingContext = new HashMap<>();
        this.controllersMap = controllersMap;
        controllersMap.keySet().forEach(this::registerController);
    }

    public static void init() {
        if (instance != null) return;

        final Map<Class, Object> controllersMap = new Reflections(SEARCH_PATH).getTypesAnnotatedWith(Controller.class)
                .stream().collect(toMap(c -> c, DispatcherContext::instantiate));

        instance = new DispatcherContext(controllersMap);
    }

    public static Supplier<String> getInvocation(final String mapping) throws EndpointNotFoundException {
        return instance.getMappedInvocation(mapping);
    }

    private Supplier<String> getMappedInvocation(final String mapping) throws EndpointNotFoundException {
        final Method methodToInvoke = mappingContext.get(mapping);

        if (methodToInvoke == null) {
            logger.error("Unable to find mapping for " + mapping);
            throw new EndpointNotFoundException("Endpoint for " + mapping + " not exist.");
        }

        return fromMethodToSupplier(methodToInvoke);
    }

    private void registerController(final Class controller) {
        logger.info("Start registering " + controller.getName() + " controller...");

        final List<Method> methods = Arrays.stream(controller.getDeclaredMethods())
                .filter(m -> isPublic(m.getModifiers()) && m.isAnnotationPresent(RequestMapping.class)).collect(toList());

        if (methods.size() == 0) {
            logger.warn("There are no methods for " + controller.getName() + " controller.");
        }

        if (controller.getAnnotation(RequestMapping.class) == null) {
            methods.forEach(m ->
                    {
                        String mapping = (m.getAnnotation(RequestMapping.class)).value();

                        if (StringUtils.isBlank(mapping)) {
                            logger.error("Failed to register endpoint with empty mapping.");
                            throw new ContextInitializationException("Mapping for " + m.getName() + " from " + controller.getName() + " class is empty.");
                        }

                        registerEndpoint(mapping, m);
                    });
            return;
        }

        methods.forEach(m ->
                {
                    String mapping =
                            ((RequestMapping) controller.getAnnotation(RequestMapping.class)).value() +
                                    (m.getAnnotation(RequestMapping.class)).value();

                    if (StringUtils.isBlank(mapping)) {
                        logger.error("Failed to register endpoint with empty mapping.");
                        throw new ContextInitializationException("Mapping for " + m.getName() + " from " + controller.getName() + " class is empty.");
                    }

                    registerEndpoint(mapping, m);
                });
    }

    private void registerEndpoint(final String mapping, final Method method) {
        logger.info("Start registering " + mapping + " endpoint for " + method.getDeclaringClass().getName() + " controller...");
        if (mappingContext.containsKey(mapping)) {
            logger.error("Mapping '" + mapping + "' already registered.");
            throw new ContextInitializationException(mappingContext + " already exists in context.");
        }
        mappingContext.put(mapping, method);
    }

    private Supplier<String> fromMethodToSupplier(final Method method) {
        return () ->
        {
            try {
                return method.invoke(controllersMap.get(method.getDeclaringClass())).toString();
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.error(e.getMessage());
                throw new RuntimeException(e);
            }
        };
    }

    private static Object instantiate(final Class clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error(e.getMessage());
            throw new ContextInitializationException("Unable to instantiate " + clazz.getName(), e.getCause());
        }
    }
}

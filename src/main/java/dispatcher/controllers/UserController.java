package dispatcher.controllers;

import dispatcher.annotations.Controller;
import dispatcher.annotations.RequestMapping;

/**
 * @author Ruslan Molchanov
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/hello")
    public String user() {
        return "Hello USER!";
    }

}

package dispatcher.controllers;

import dispatcher.annotations.Controller;
import dispatcher.annotations.RequestMapping;

/**
 * @author Ruslan Molchanov
 */
@Controller
public class BaseController {

    @RequestMapping("/hello")
    public String hello() {
        return "<html>" +
                "<body>" +
                "Hello!" +
                "</body>" +
                "</html";
    }

    @RequestMapping("/")
    public String index() {
        return "Index";
    }

}

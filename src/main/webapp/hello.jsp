<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Home</title>
</head>
<body>

<h3>Hello from JSP!</h3>

<c:set var="lines" value="${['line1', 'line2', 'line3']}"/>

<c:forEach var="line" items="${lines}">
    <p>${line}</p>
</c:forEach>

</body>
</html>
